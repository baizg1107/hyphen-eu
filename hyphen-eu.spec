Name:                hyphen-eu
Version:             0.20110620
Release:             14
Summary:             Basque hyphenation rule
License:             LPPL
URL:                 http://tp.lc.ehu.es/jma/basque.html
Source:              http://tug.ctan.org/tex-archive/language/hyph-utf8/tex/generic/hyph-utf8/patterns/tex/hyph-eu.tex
BuildArch:           noarch
BuildRequires:       hyphen-devel
Requires:            hyphen
Supplements:         (hyphen and langpacks-eu)
Patch0:              hyphen-eu-cleantex.patch
%description
hyphen-eu provides support for the Basque language under the Babel package for LaTeX2e

%prep
%setup -T -q -c -n hyphen-eu
cp -p %{SOURCE0} .
%patch0 -p0 -b .clean

%build
grep -v "^%" hyph-eu.tex | tr ' ' '\n' > temp.tex
substrings.pl temp.tex hyph_eu_ES.dic ISO8859-1
cat <<EOF> README
Created with substring.pl by substrings.pl hyph-eu.tex hyph_eu_ES.dic ISO8859-1
---
EOF
head -n 34 hyph-eu.tex >> README

%install
install -Dp hyph_eu_ES.dic $RPM_BUILD_ROOT/%{_datadir}/hyphen/hyph_eu_ES.dic

%files
%doc README
%{_datadir}/hyphen/*

%changelog
* Mon Jul 6 2020 yanan li <liyanan032@huawei.com> - 0.%{upstreamid}-14
- Package init
